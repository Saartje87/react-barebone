import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './layout.css';
import FooterNavigation from 'components/FooterNavigation';

export default class Layout extends Component {
	static propTypes = {
		children: PropTypes.node.isRequired,
	};

	render() {
		const { children } = this.props;

		return (
			<div className="layout">
				<div className="layout-body">
					{ children }
				</div>
				<FooterNavigation />
			</div>
		);
	}
}
