import React, { Component } from 'react';

import './home.css';
import Paper from 'components/Paper';

export default class Home extends Component {
	state = {
		open: false,
	};

	render() {
		return (
			<div className="home">
				<h1>SCHEDULE</h1>

				<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/16-9-Logo.svg/2000px-16-9-Logo.svg.png" alt="" />

				<button onClick={ () => { this.setState({ open: true }); } }>Tap to open paper</button>

				<Paper overlay effect="clip" open={ this.state.open }>
					<h1>Paper overlay</h1>

					<button onClick={ () => { this.setState({ open: false }); } }>Tap to close paper</button>
				</Paper>
			</div>
		);
	}
}
