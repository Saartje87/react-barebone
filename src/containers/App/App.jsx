import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import {
  HashRouter,
  Route,
} from 'react-router-dom';

import Layout from 'containers/Layout';
import Home from 'containers/Home';
import Leaderboards from 'containers/Leaderboards';
import Predictions from 'containers/Predictions';
import Leagues from 'containers/Leagues';
import More from 'containers/More';
import Paper from 'components/Paper';

export default class App extends Component {
	static propTypes = {
		store: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
	};

	shouldComponentUpdate() {
		return false;
	}

	render() {
		const { store } = this.props;

		return (
			<Provider store={ store }>
				<HashRouter>
					<Layout>
						<Route exact path="/">
							{ ({ match }) => <Paper fit open={ !!match }><Home /></Paper> }
						</Route>
						<Route path="/leaderboards">
							{ ({ match }) => <Paper fit open={ !!match }><Leaderboards /></Paper> }
						</Route>
						<Route path="/predictions">
							{ ({ match }) => <Paper fit open={ !!match }><Predictions /></Paper> }
						</Route>
						<Route path="/leagues">
							{ ({ match }) => <Paper fit open={ !!match }><Leagues /></Paper> }
						</Route>
						<Route path="/more">
							{ ({ match }) => <Paper fit open={ !!match }><More /></Paper> }
						</Route>
					</Layout>
				</HashRouter>
			</Provider>
		);
	}
}
