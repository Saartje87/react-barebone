import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './footer-navigation.css';

export default class App extends Component {
	render() {
		return (
			<div className="footer-navigation">
				<NavLink activeClassName="is-selected" exact to="/">Schedule</NavLink>
				<NavLink activeClassName="is-selected" to="/leaderboards">Leaderboards</NavLink>
				<NavLink activeClassName="is-selected" to="/predictions">Predictions</NavLink>
				<NavLink activeClassName="is-selected" to="/leagues">Leagues</NavLink>
				<NavLink activeClassName="is-selected" to="/more">More</NavLink>
			</div>
		);
	}
}
