import React, { PureComponent } from 'react';
import {
	unstable_renderSubtreeIntoContainer as renderSubtreeIntoContainer,
	unmountComponentAtNode,
} from 'react-dom';
import PropTypes from 'prop-types';
import anime from 'animejs';

import './paper.css';
import * as effects from './effects';

export default class Paper extends PureComponent {
	static propTypes = {
		children: PropTypes.node.isRequired,
		effect: PropTypes.oneOf(['fade', 'bounce', 'slide', 'clip']),
		fit: PropTypes.bool,
		flex: PropTypes.bool,
		open: PropTypes.bool,
		overlay: PropTypes.bool,
		scroll: PropTypes.bool,
	};

	static defaultProps = {
		effect: 'fade',
		fit: false,
		flex: false,
		open: true,
		overlay: false,
		scroll: false,
	};

	// eslint-disable-next-line react/sort-comp
	animation = null;

	state = {
		render: true,
	};

	componentWillMount() {
		this.setState({
			render: this.props.open,
		});
	}

	componentDidMount() {
		if (this.state.render) {
			this.show();
		}
	}

	componentWillUnmount() {
		if (this.props.overlay && this.node) {
			this.componentWillLeave(() => {
				if (this.props.overlay) {
					this.unrenderLayer();
				}
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		const { open } = nextProps;

		if (this.props.open !== open) {
			if (this.animation) {
				if (this.animation.cancel) {
					this.animation.cancel();
				} else {
					this.animation.pause();
				}
			}

			if (open) {
				this.setState({
					render: true,
				}, this.show);
			} else {
				this.hide();
			}
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		return this.state.render !== nextState.render;
	}

	componentDidUpdate() {
		if (this.props.overlay) {
			this.renderLayer();
		}
	}

	show() {
		if (this.props.overlay) {
			this.renderLayer();
		}

		if (this.node) {
			this.animation = this.componentWillEnter();
		}
	}

	hide() {
		if (!this.node) {
			this.setState({
				render: false,
			});

			return;
		}

		this.animation = this.componentWillLeave(() => {
			this.setState({
				render: false,
			});

			if (this.props.overlay) {
				this.unrenderLayer();
			}
		});
	}

	componentWillEnter() {
		const effect = effects[this.props.effect];

		return anime({
			targets: [this.node],
			...effect.enter,
		});
	}

	componentWillLeave(complete) {
		const effect = effects[this.props.effect];

		return anime({
			targets: [this.node],
			...effect.leave,
			complete,
		});
	}

	renderLayer() {
		if (!this.layer) {
			this.layer = document.createElement('div');
			document.body.appendChild(this.layer);
		}

		const {
			children,
			scroll,
		} = this.props;

		const child = (
			<div
				className={ `paper is-fit ${scroll ? 'is-scroll' : ''}` }
				ref={ (node) => { this.node = node; } }
			>
				{ this.props.children }
			</div>
		);

		this.component = renderSubtreeIntoContainer(this, child, this.layer);
	}

	unrenderLayer() {
		if (!this.layer) {
			return;
		}

		unmountComponentAtNode(this.layer);
		document.body.removeChild(this.layer);
		this.layer = null;
	}

	render() {
		const {
			children,
			fit,
			flex,
			scroll,
		} = this.props;

		if (!this.state.render || this.props.overlay) {
			return null;
		}

		return (
			<div
				className={ `paper ${fit ? 'is-fit' : ''} ${flex ? 'is-flex' : ''} ${scroll ? 'is-scroll' : ''}` }
				ref={ (node) => { this.node = node; } }
			>
				{ children }
			</div>
		);
	}
}
