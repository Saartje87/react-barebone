import anime from 'animejs';

export const fade = {
	enter: {
		opacity: {
			value: [0, 1],
			easing: 'easeInOutSine',
		},
		duration: 600,
	},
	leave: {
		opacity: 0,
		easing: 'easeInOutSine',
		duration: 400,
	},
};

export const bounce = {
	enter: {
		opacity: {
			value: [0, 1],
			easing: 'easeInOutSine',
			duration: 400,
		},
		translateY: ['-20%', 0],
		duration: 800,
	},
	leave: {
		opacity: 0,
		easing: 'easeInOutSine',
		duration: 400,
	},
};

export const slide = {
	enter: {
		opacity: {
			value: [0, 1],
		},
		translateX: ['100%', 0],
		easing: 'easeOutQuad',
		duration: 200,
	},
	leave: {
		opacity: 0,
		translateX: '100%',
		easing: 'easeInOutSine',
		duration: 200,
	},
};

// Mhh anime doesn;t support clip-path?
export const clip = {
	enter: {
		opacity: [0, 1],
		scale: [0, 1],
		easing: 'easeOutQuad',
		duration: 200,
	},
	leave: {
		opacity: 0,
		scale: 0,
		easing: 'easeInOutSine',
		duration: 200,
	},
};
