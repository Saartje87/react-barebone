module.exports = {
	"env": {
		"browser": true,
		"jasmine": true,
	},
	"parser": "babel-eslint",
	"parserOptions": {
		"sourceType": "module",
		"allowImportExportEverywhere": false
	},
	"extends": "airbnb",
	"plugins": [
		"react",
		"jsx-a11y",
		"import"
	],
	"rules": {
		"class-methods-use-this": "off",
		"import/extensions": "off",
		"import/no-extraneous-dependencies": "off",
		"import/no-unresolved": [2, { ignore: ['^(components|actions|reducers|config|containers|static|selectors|api)'] }],
		"import/prefer-default-export": "off",
		"import/first": "off",
		"indent": ["error", "tab", { "SwitchCase": 1 }],
		"max-len": "off",
		"no-tabs": "off",
		"quotes": [2, "single", { "allowTemplateLiterals": true }],
		"react/jsx-curly-spacing": [1, "always"],
		"react/jsx-indent-props": ["error", "tab"],
		"react/jsx-indent": ["error", "tab"],
		"react/prefer-stateless-function": "off",
		"react/no-string-refs": "off",									// TODO Should be enabled
		"no-underscore-dangle": [2, { "allowAfterThis": true }],
		"no-use-before-define": "off",
		"no-shadow": "warn",

		// "no-undef": [2, { "__DEV__": true }],
		// sort-comp, waiting for class properties --> https://github.com/yannickcr/eslint-plugin-react/pull/685
	},
	"globals": {
		"__DEV__": true,
		"__PRODUCTION__": true,
		"jest": true,
	}
};
