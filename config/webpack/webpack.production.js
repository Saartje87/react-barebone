const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// https://webpack.js.org/configuration/
module.exports = {
	target: 'web',
	// TODO Which is the best fit?
	devtool: 'source-map',
	context: path.join(__dirname, '../../src'),
	entry: {
		app: './main',
		vendor: [
			'animejs',
			'prop-types',
			'react-dom',
			'react-redux',
			'react-router-dom',
			'react',
			'redux',
		],
	},
	resolve: {
		modules: [
			'node_modules',
			path.resolve(__dirname, '../../src'),
		],
		extensions: ['.js', '.json', '.jsx'],
	},
	output: {
		path: path.join(__dirname, '../../dist'),
		filename: 'bundle.js',
	},
	performance: {
		hints: false, // 'warning' | 'error' | false
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				options: {
					presets: [['es2015', { modules: false }], 'stage-0', 'stage-1', 'react'],
					plugins: ['transform-decorators-legacy'],
					cacheDirectory: false,
				},
			},
			{
				test: /\.html$/,
				loader: 'html-loader',
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						'css-loader?sourceMap',
						'postcss-loader',
					],
				}),
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: 'index.html',
			filename: 'index.html',
			hash: false,
		}),
		new webpack.NamedModulesPlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			minChunks: Infinity,
			filename: 'vendor.bundle.js',
		}),
		new webpack.optimize.CommonsChunkPlugin({
			async: true,
			children: true,
			minChunks: 4,
		}),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production'),
			},
			__DEV__: JSON.stringify(false),
		}),
		new ExtractTextPlugin({
			filename: 'styles.css',
			allChunks: true,
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false,
			options: {
				context: __dirname,
			},
		}),
	],
};
