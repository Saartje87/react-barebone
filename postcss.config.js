module.exports = {
	plugins: [
		require('postcss-import')(), // eslint-disable-line global-require
		require('postcss-cssnext')({ // eslint-disable-line global-require
			browsers: [
				'Android >= 5.0',
				'iOS >= 8.0',
			],
		}),
		require('postcss-reporter')({ clearMessages: true }), // eslint-disable-line global-require
		require('precss')(), // eslint-disable-line global-require
	],
};
